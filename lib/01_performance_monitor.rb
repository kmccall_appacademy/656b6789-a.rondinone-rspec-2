def measure(num = 1, &block)
  start_time = Time.now
  num.times { block.call }
  (Time.now - start_time) / num
end
