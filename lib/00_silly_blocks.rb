def reverser(&block)
  words = block.call.split(" ")
  words.map! {|i| i.reverse}.join(" ")
end

def adder(num = 1)
  n = yield
  n + num
end

def repeater(num = 1, &block)
  num.times {block.call}
end
